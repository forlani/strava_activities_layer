# StravaActivityLayer QGIS2 plugin
## What is it?
This plugin takes a Strava activities.zip file as input and, depending on user's choice, can output: tracks, segments, points.
One single vector table is produced containing the entire activities.zip data.
### Tracks
These represent whole activities e.g. an entire ride or run.
The table contains basic information such as the name of the gpx file (that's what is in the activity.zip) the ride name, start time, elevation gain, etc..
### Segments
These represent gps point to gps point linestring. 
The table contains a rich set of attributes such as: start/end time of each segment, length, cumulative length, biometrics: heart rate, cadence, watts (sensor outputs)
### Points
This is very similar to segments but has only pure gps points as captured by the user's device.
## How to obtain the data
Data will have to be requested through the user's own Starva account --> Settings --> My Profile --> Download all your activities. 
## Testing
An activities.zip file is included here for testing purposes. It is limited to only three activities. The plugin can handle numerous activities.
