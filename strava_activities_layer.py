# -*- coding: utf-8 -*-
"""
/***************************************************************************
 StravaActivitiesLayer
                                 A QGIS plugin
 This plugin takes a Strava activities.zip file and convert it to a map layer.
                              -------------------
        begin                : 2018-04-02
        git sha              : $Format:%H$
        copyright            : (C) 2018 by Armando Forlani
        email                : armando.forlani@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt4.QtCore import  QSettings, QTranslator, qVersion, QCoreApplication, Qt, QVariant, QDateTime #AF added Qt for align progress bar
from PyQt4.QtGui import QAction, QIcon, QMessageBox, QFileDialog, QProgressBar   # AF added. Remove if not used
# Initialize Qt resources from file resources.py
import resources
# Import the code for the dialog
from strava_activities_layer_dialog import StravaActivitiesLayerDialog

import os.path

from qgis.core import QgsCoordinateReferenceSystem, QgsDistanceArea, QgsField, QgsVectorLayer, QgsPoint, QgsFeature, QgsGeometry, QgsMapLayerRegistry, QgsMessageLog, QGis, QgsVectorFileWriter

import zipfile
import xml


class StravaActivitiesLayer:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgisInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'StravaActivitiesLayer_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)


        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&Strava Activities to QGIS')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'StravaActivitiesLayer')
        self.toolbar.setObjectName(u'StravaActivitiesLayer')


    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('StravaActivitiesLayer', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        # Create the dialog (after translation) and keep reference
        self.dlg = StravaActivitiesLayerDialog()

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToVectorMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/StravaActivitiesLayer/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Strava to layer'),
            callback=self.run,
            parent=self.iface.mainWindow())

        # Initializes dialog widgets.
        self.dlg.setFixedSize(self.dlg.size())
        # Locates activities file.
        self.dlg.le_open.setPlaceholderText("[activities.zip]")
        self.dlg.pb_open.clicked.connect(self.getfilename_open)
        self.dlg.pb_open.setFocus(True)
        # Activities date range: default not enabled.
        self.dlg.lb_from.setEnabled(False)
        self.dlg.lb_to.setEnabled(False)
        self.dlg.rb_allactivities.setChecked(True)
        # Radio to enable date range input.
        self.dlg.rb_allactivities.toggled.connect(self.enable_date)
        self.dlg.dte_from.setEnabled(False)
        self.dlg.dte_to.setEnabled(False)
        self.dlg.dte_from.setCalendarPopup(True)
        self.dlg.dte_to.setCalendarPopup(True)
        # Set range to today. Overwritten by activities file date range when user locates .zip file.
        self.dlg.dte_from.setDateTime(QDateTime.currentDateTime())
        self.dlg.dte_to.setDateTime(QDateTime.currentDateTime())
        # Default output vector type is the whole activity i.e. a single linestring.
        self.dlg.rb_tracks.setChecked(True)
        # Output vector to memory by default.
        self.dlg.rb_memory.setChecked(True)
        self.dlg.rb_memory.toggled.connect(self.enable_le_save)
        self.dlg.le_save.setEnabled(False)
        # When output vector type is file, gpkg is by default.
        self.dlg.le_save.setPlaceholderText("[GeoPackage file (*.gpkg)]")
        self.dlg.pb_save.setEnabled(False)
        self.dlg.pb_save.clicked.connect(self.getfilename_save)

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginVectorMenu(
                self.tr(u'&Strava Activities to QGIS'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar

    def run(self):
        """Run method that performs all the real work"""
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result:
            # Do something useful here - delete the line containing pass and
            # substitute with your code.
            if self.dlg.le_open.text() != "":
                # In case the user is prompt for file save but chooses not to, the main dialog is set back to save to memory.
                if self.dlg.rb_file.isChecked() and self.dlg.le_save.text() == "":
                    self.dlg.rb_file.setChecked(False)
                # Run the application once the requirements are satisfied.
                self.run_processing()

    def getfilename_open(self):
        dlg = QFileDialog()
        dlg.setAcceptMode(QFileDialog.AcceptOpen)
        dlg.setFileMode(QFileDialog.ExistingFile)
        dlg.setFilter("Zip file (*.zip)")
        if dlg.exec_():
            self.dlg.le_open.clear()
            self.dlg.le_open.insert(dlg.selectedFiles()[0])
            # Set calendar date range by all activities range.
            date_list = [self.getdatetime_from_filename(value) for value in self.getziparchive_from_zipfile(self.dlg.le_open.text()).namelist()]
            self.dlg.dte_from.setDateTimeRange(min(date_list), max(date_list))
            self.dlg.dte_from.setDateTime(min(date_list))
            self.dlg.dte_to.setDateTimeRange(min(date_list), max(date_list))
            self.dlg.dte_to.setDateTime(max(date_list))

    def getfilename_save(self):

        def validate_filename(selected_files, selected_extension):
            # Set allowable extensions / file type.
            if "*.shp" in selected_extension:
                selected_ext = ".shp"
            else:
                selected_ext = ".gpkg"

            # If no file is selected do nothing else deals with other possibilities.
            if len(selected_files) == 0:
                return (None, False)
            else:
                # Missing file name
                if os.path.splitext(selected_files[0])[0] == 0:
                    return (None, False)
                # Valid
                if selected_ext in  os.path.splitext(selected_files[0])[1].lower():  # [0] file path/name; [1] extension
                    return (selected_files[0], True)
                # Invalid extension
                elif len(os.path.splitext(selected_files[0])[1].lower()) > 0:
                    return (os.path.splitext(selected_files[0])[0] + selected_ext, False)
                # Missing extension
                else:
                    return (selected_files[0] + selected_ext, False)

        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        dlg.setAcceptMode(QFileDialog.AcceptSave)
        dlg.setFilter("GeoPackage file (*.gpkg);;Shape file (*.shp)")
        if dlg.exec_():
            self.dlg.le_save.clear()
            self.dlg.le_save.insert(validate_filename(dlg.selectedFiles(), dlg.selectedNameFilter())[0])
        elif self.dlg.le_save.text() == "":
            self.dlg.rb_file.setChecked(False)

    def enable_date(self):
        if self.dlg.rb_allactivities.isChecked():
            self.dlg.lb_from.setEnabled(False)
            self.dlg.lb_to.setEnabled(False)
            self.dlg.dte_from.setEnabled(False)
            self.dlg.dte_to.setEnabled(False)
        else:
            self.dlg.lb_from.setEnabled(True)
            self.dlg.lb_to.setEnabled(True)
            self.dlg.dte_from.setEnabled(True)
            self.dlg.dte_to.setEnabled(True)

    def enable_le_save(self):
        if self.dlg.rb_file.isChecked():
            self.dlg.le_save.setEnabled(True)
            self.dlg.pb_save.setEnabled(True)
        else:
            self.dlg.le_save.setEnabled(False)
            self.dlg.pb_save.setEnabled(False)

    def getdatetime_from_filename(self, name):
        return QDateTime.fromString(name[:15], "yyyyMMdd'-'hhmmss")  # 20180325-093028-Ride.gpx   20180325-093028-Run.gpx

    def getziparchive_from_zipfile(self, zip_file):
        try:
            archive = zipfile.ZipFile(zip_file, 'r')
        except zipfile.BadZipfile:
            QgsMessageLog.logMessage("BadZipfile: {} is not a valid zip file".format(os.path.basename(zip_file)), level=QgsMessageLog.CRITICAL)
        except zipfile.LargeZipFile:
            QgsMessageLog.logMessage("LargeZipFile: {} is too large".format(os.path.basename(zip_file)), level=QgsMessageLog.CRITICAL)
        except:
            QgsMessageLog.logMessage("Something went wrong opening {}".format(os.path.basename(zip_file)), level=QgsMessageLog.CRITICAL)
        else:
            return archive

    def run_processing(self):

        def getactivitydict():

            def getdatetime_from_gpspoint(gps_time):
                return QDateTime.fromString(gps_time, "yyyy'-'MM'-'dd'T'hh':'mm':'ss'Z'")

            def getactivity_type(name):
                if name[-7:-4].lower() == 'run':
                    return 'run'
                elif name[-8:-4].lower() == 'ride':
                    return 'ride'

            archive = self.getziparchive_from_zipfile(self.dlg.le_open.text())

            if self.dlg.rb_allactivities.isChecked():
                names_range = archive.namelist()
            else:
                # Read user date range selection.
                daterange_from = self.dlg.dte_from.dateTime()
                daterange_to = self.dlg.dte_to.dateTime()
                names_range = [name for name in archive.namelist() if self.getdatetime_from_filename(name) >= daterange_from and self.getdatetime_from_filename(name) <= daterange_to]

            progress.setMaximum(len(names_range))

            for idx, member_name in enumerate(names_range):
                progress.setValue(idx + 1)
                status.showMessage("Processing: {} ".format(member_name))
                gpx = archive.read(member_name)
                # Read each gpx file into a dict. Each gpx file contains a single ride.
                trkpoints = []
                root = xml.etree.ElementTree.fromstring(gpx)
                # root[0] contains the metadata: timestamp.
                # root[1] contains the data: the track (can be absent).
                if len(root) < 2:
                    QgsMessageLog.logMessage("Missing track data. GPX file: {}".format(member_name), level=QgsMessageLog.WARNING)
                else:
                    activity_type = getactivity_type(member_name)
                    for trkpt in root[1][1]:
                        trkpoint = {"point": QgsPoint(float(trkpt.attrib['lon']), float(trkpt.attrib['lat'])),
                                    "elev": float(trkpt[0].text),
                                    "time": getdatetime_from_gpspoint(trkpt[1].text),
                                    "temp": None,
                                    "cad": None,
                                    "hr": None,
                                    "watts": None}
                        # Deals with gpx extensions
                        if len(trkpt) > 2:
                            # Extension can have: temp, cad, etc... add the rest when it becomes known.
                            #  The first and only element of extensions contains the actual extensions.
                            #  What specific extension may be present is unknown until accessed.
                            extensions = trkpt[2][0] 
                            for extension in extensions:
                                if extension.tag[-5:] == 'atemp':
                                    trkpoint["temp"] = int(extension.text)
                                if extension.tag[-3:] == 'cad':
                                    if activity_type == 'run':
                                        trkpoint["cad"] = int(extension.text) * 2
                                    else:
                                        trkpoint["cad"] = int(extension.text)
                                if extension.tag[-2:] == 'hr':
                                    trkpoint["hr"] = int(extension.text)
                                if extension.tag[-5:] == 'watts':
                                    trkpoint["watts"] = int(extension.text)
                        trkpoints.append(trkpoint)

                    yield {'file_name':os.path.basename(member_name), 'name':root[1][0].text, 'type':activity_type, 'time':getdatetime_from_gpspoint(root[0][0].text), 'track':trkpoints} 

        def getfeature_from_activity(rides):
            # There should only be a single activity per gpx file. This is to be confirmed.
            # Until then, assume there could be multiple activities.
            for ride in rides:
                points = []
                prev_trkpoint = None
                tot_dist = 0
                elev_gain = 0
                duration = 0
                for trkpoint in ride['track']:

                    time = trkpoint['time']

                    points.append(trkpoint['point'])
                    feat = QgsFeature()

                    if out_type == 'points':
                        feat.setGeometry(QgsGeometry.fromPoint(points[-1]))

                        if prev_trkpoint != None:
                            tot_dist += distarea.measureLength(QgsGeometry.fromPolyline([points[-2], points[-1]]))

                        feat.setAttributes([
                            ride['file_name'], 
                            ride['name'], 
                            ride['type'], 
                            len(points), 
                            trkpoint['time'], 
                            tot_dist, 
                            trkpoint['elev'], 
                            trkpoint['temp'], 
                            trkpoint['cad'], 
                            trkpoint['hr'], 
                            trkpoint['watts']])

                        yield feat

                    elif out_type == 'segments':
                        if prev_trkpoint != None:
                            feat.setGeometry(QgsGeometry.fromPolyline([points[-2], points[-1]]))
                            dist = distarea.measureLength(feat.geometry())
                            if dist == 0: dist = 0.01
                            tot_dist += dist
                            feat.setAttributes([
                                ride['file_name'], 
                                ride['name'], 
                                ride['type'], 
                                len(points)-1,
                                prev_trkpoint['time'],
                                trkpoint['time'],
                                dist,
                                tot_dist, 
                                prev_trkpoint['elev'],
                                trkpoint['elev'], 
                                trkpoint['temp'], 
                                prev_trkpoint['cad'], 
                                prev_trkpoint['hr'], 
                                prev_trkpoint['watts']])

                            yield feat

                    # Accumulate elevation gain for the track.
                    if prev_trkpoint != None:
                        if trkpoint['elev'] > prev_trkpoint['elev']:
                            elev_gain += trkpoint['elev'] - prev_trkpoint['elev']

                    prev_trkpoint = trkpoint

                if out_type == 'track':
                    feat.setGeometry(QgsGeometry.fromPolyline(points))
                    feat.setAttributes([
                        ride['file_name'], 
                        ride['name'], 
                        ride['type'], 
                        ride['time'], 
                        prev_trkpoint['time'],
                        elev_gain,
                        distarea.measureLength(feat.geometry())])

                    yield feat

        def getvectorlayer():

            points_fields = [
                QgsField("GPX_FILE_NAME", QVariant.String),
                QgsField("ACTIVITY_NAME", QVariant.String),
                QgsField("ACTIVITY_TYPE", QVariant.String),
                QgsField("SEQUENCE", QVariant.Int),
                QgsField("TIME", QVariant.DateTime),
                QgsField("CUMULATIVE_DIST_M", QVariant.Double, "double", 8, 2),
                QgsField("ELEVATION_M", QVariant.Double, "double", 8, 2),
                QgsField("TEMPERATURE_C", QVariant.Int),
                QgsField("CADENCE", QVariant.Int),
                QgsField("HEART_RATE", QVariant.Int),
                QgsField("WATTS", QVariant.Int)]

            segments_fields = [
                QgsField("GPX_FILE_NAME", QVariant.String),
                QgsField("ACTIVITY_NAME", QVariant.String),
                QgsField("ACTIVITY_TYPE", QVariant.String),
                QgsField("SEQUENCE", QVariant.Int),
                QgsField("START_TIME", QVariant.DateTime),
                QgsField("END_TIME", QVariant.DateTime),
                QgsField("DISTANCE_M", QVariant.Double, "double", 8, 2),
                QgsField("CUMULATIVE_DIST_M", QVariant.Double, "double", 8, 2),
                QgsField("START_ELEVATION_M", QVariant.Double, "double", 8, 2),
                QgsField("END_ELEVATION_M", QVariant.Double, "double", 8, 2),
                QgsField("TEMPERATURE_C", QVariant.Int),
                QgsField("CADENCE", QVariant.Int),
                QgsField("HEART_RATE", QVariant.Int),
                QgsField("WATTS", QVariant.Int)]

            track_fields = [
                QgsField("GPX_FILE_NAME", QVariant.String),
                QgsField("ACTIVITY_NAME", QVariant.String),
                QgsField("ACTIVITY_TYPE", QVariant.String),
                QgsField("START_TIME", QVariant.DateTime),
                QgsField("END_TIME", QVariant.DateTime),
                QgsField("ELEVATION_GAIN_M", QVariant.Double, "double", 8, 2),
                QgsField("DISTANCE_M", QVariant.Double, "double", 8, 2)]

            if out_type == 'points':
                fields = points_fields
                vl_mem = QgsVectorLayer("Point?crs=" + epsgcode, "temporary_vector", "memory")
            elif out_type == 'segments':
                fields = segments_fields
                vl_mem = QgsVectorLayer("Linestring?crs=" + epsgcode, "temporary_vector", "memory")
            elif out_type == 'track':
                fields = track_fields
                vl_mem = QgsVectorLayer("Linestring?crs=" + epsgcode, "temporary_vector", "memory")

            pr_mem = vl_mem.dataProvider()
            vl_mem.startEditing()
            pr_mem.addAttributes(fields)
            vl_mem.commitChanges()

            return vl_mem, pr_mem

        def writetofile(vl_mem, pr_mem, file_name):
            QgsMessageLog.logMessage("writing to vector..", level=QgsMessageLog.INFO)
            progress.setValue(0)
            progress.setMaximum(vl_mem.featureCount())

            filename, file_extension = os.path.splitext(file_name)

            if out_type == 'points':
                geom_type = QGis.WKBPoint
            elif out_type == 'segments' or out_type == 'track':
                geom_type = QGis.WKBLineString

            if file_extension.lower() == ".gpkg":
                filewriter = QgsVectorFileWriter(filename, pr_mem.encoding(), pr_mem.fields(), geom_type, crs, driverName="GPKG")
            elif file_extension.lower() == ".shp":
                filewriter = QgsVectorFileWriter(filename, pr_mem.encoding(), pr_mem.fields(), geom_type, crs, driverName="ESRI Shapefile")

            status.showMessage("Saving features to file...")

            for idx, feat in enumerate(pr_mem.getFeatures()):
                progress.setValue(idx)
                filewriter.addFeature(feat)

            del filewriter

        progress_messagebar = self.iface.messageBar().createMessage("Converting activities file to vector...")
        progress = QProgressBar()

        progress.setAlignment(Qt.AlignLeft|Qt.AlignVCenter)
        progress_messagebar.layout().addWidget(progress)

        status = self.iface.mainWindow().statusBar()
        
        self.iface.messageBar().pushWidget(progress_messagebar, self.iface.messageBar().INFO)

        out_type = ''
        if self.dlg.rb_tracks.isChecked():
            out_type = 'track'
        elif self.dlg.rb_segments.isChecked():
            out_type = 'segments'
        else:
            out_type = 'points'

        epsgcode = "EPSG:4326"
        crs = QgsCoordinateReferenceSystem(epsgcode)
        distarea = QgsDistanceArea()
        distarea.setSourceCrs(crs)
        distarea.setEllipsoid(crs.ellipsoidAcronym())
        distarea.setEllipsoidalMode(crs.geographicFlag())

        rides_layer, rides_provider = getvectorlayer()

        for feat in getfeature_from_activity(getactivitydict()):
            rides_provider.addFeatures([feat])

        if rides_layer.isValid() and rides_layer.featureCount() > 0:
            rides_layer.commitChanges()
            rides_layer.updateExtents()

            saveas = self.dlg.le_save.text()
            filename, file_extension = os.path.splitext(saveas)

            if self.dlg.rb_file.isChecked():
                writetofile(rides_layer, rides_provider, saveas)  # TODO pass layer only!!
                # Add layer from file.
                self.iface.addVectorLayer(saveas, os.path.basename(filename), "ogr")
            else:
                # Add layer from memory.
                QgsMapLayerRegistry.instance().addMapLayer(rides_layer)
        else:
            QgsMessageLog.logMessage("Vector layer ignored.", level=QgsMessageLog.WARNING)
            del rides_layer
            
        self.iface.messageBar().clearWidgets()
        status.clearMessage()

        

