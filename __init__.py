# -*- coding: utf-8 -*-
"""
/***************************************************************************
 StravaActivitiesLayer
                                 A QGIS plugin
 This plugin takes a Strava activities.zip file and convert it to a map layer.
                             -------------------
        begin                : 2018-04-02
        copyright            : (C) 2018 by Armando Forlani
        email                : me@me.me
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load StravaActivitiesLayer class from file StravaActivitiesLayer.

    :param iface: A QGIS interface instance.
    :type iface: QgisInterface
    """
    #
    from .strava_activities_layer import StravaActivitiesLayer
    return StravaActivitiesLayer(iface)
